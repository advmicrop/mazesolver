/*
 * rbmotor.h
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */
/*
 * For realization of motor speeds and directions
 */
#ifndef RBMOTOR_H
#define RBMOTOR_H
#include "rbstate.h"

/**
 * API - Initialize pins mode and start chibi hal pwm driver
 */
void rbMotorInit(void);

/**
 * API - Adjust max duty cycle possible
 * Input: unsigned int (20-100)
 * Output: false - invalid value; true - success
 * Modify: maxPWM
 */
bool rbMotorSpdLim(unsigned int spd);


/**
 * API - Call functions to change direction and power to motors
 * Input: reference to rbState object
 */
void rbMotorSet(rbstate_t const *rbStatep);

#endif /* RBMOTOR_H */
