/*
 * rbdistance.c
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */
/**
*  Sharp GP2Y0A41SK0F Analog Distance Sensor 4-30cm
*  https://www.pololu.com/product/2464
*/

#include "rbdistancesensor.h"
#include "rbpindefinition.h"
#include "hal.h"

#define DISTAN_ADC_BUF_DEPTH    10
#define VOLTRESOL               3.0f/4096.0f      // ADC voltage resolution (12bits)

/**
*  Linearization of data based on datasheet formula 12/Voltage - 0.42,
*  but with a little adjustment for more accurate result
*  Input: raw ADC value
*  Output: distance in cm
*/
float _convert2cm(adcsample_t ADCVal) {
  float volt, dis;
  volt = VOLTRESOL * ADCVal;
  dis = 16.5f/volt - 0.5f;
  return dis;
}

/*
 * ADC conversion group.
 * Mode:        Linear buffer, 1 samples of 2 channel, SW triggered.
 * Channels:    See rbpindefinition.h
 */
static ADCConversionGroup adcgrpcfg = {
  .circular = FALSE,                     // One shot linear mode
  .num_channels = DISTAN_ADC_NUM_CHAN,
  .end_cb = NULL,
  .error_cb = NULL,
  .cr1 = 0,
  .cr2 = ADC_CR2_SWSTART,               // Software trigger
  .smpr1 = 0,
  .smpr2 = DISTAN_ADC_SMPR_FRONT(ADC_SAMPLE_56) |
           DISTAN_ADC_SMPR_RIGHT(ADC_SAMPLE_56), // Sampling time (+12 cycles)
  .sqr1 = 0,
  .sqr2 = 0,
  .sqr3 = ADC_SQR3_SQ2_N(DISTAN_ADC_CHAN_FRONT) |
          ADC_SQR3_SQ1_N(DISTAN_ADC_CHAN_RIGHT)  // Sampling Sequence
};

/**
 * API - Initialize pins mode and start chibi hal adc driver
 */
void rbDisADCInit(void) {
  palSetLineMode(DISTAN_PIN_FRONT, PAL_MODE_INPUT_ANALOG);
  palSetLineMode(DISTAN_PIN_RIGHT, PAL_MODE_INPUT_ANALOG);
  adcStart(&DISTAN_ADC_DRV, NULL);          // Use default config
}

/**
 * API - Start ADC sampling sequence (blocking)
 * Input: reference to disFront and disRight
 * Modify: disFront, disRight
 */
void rbDisGet(float *disFront, float *disRight) {
  adcsample_t adcBuf[DISTAN_ADC_NUM_CHAN*DISTAN_ADC_BUF_DEPTH];
  adcConvert(&DISTAN_ADC_DRV, &adcgrpcfg, adcBuf, DISTAN_ADC_BUF_DEPTH);

  /* Averaging filter */
  for (int i = 1; i < DISTAN_ADC_BUF_DEPTH; i++) {
    adcBuf[1] += adcBuf[2*i + 1];
    adcBuf[0] += adcBuf[2*i];
  }
  adcBuf[1] /= DISTAN_ADC_BUF_DEPTH;
  adcBuf[0] /= DISTAN_ADC_BUF_DEPTH;

  *disFront = _convert2cm(adcBuf[1]);
  *disRight = _convert2cm(adcBuf[0]);
}


