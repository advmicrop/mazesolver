/*
 * rbgroundsensor.c
 *
 *  Created on: Mar 16, 2019
 *      Author: Phongbach
 */
/*
 *  QTR-1RC Reflectance Sensor
 *  https://www.pololu.com/product/2459
 *  Operation:
 *  - Start sensor by configure pin as output push-pull, high.
 *  - It take about 10usec for collector voltage to charge up to steady state, due to collector capacitor.
 *  - Then pin will be set as alternate function, ICU start timer immediately
 *  - ICU will call back when detect input goes low, discharge time would be the width reported by ICU.
 *  - Time for collector voltage to go from steady state high to low threshold
 *  depends on current through collector capacitor.
 *  - This current is modulated by photo transistor, proportional to the reflected infrared light.
 *  - With a strong reflectance (white surface), the decay time can be as low as several dozen microseconds;
 *  with no reflectance (black surface), the decay time can be up to a few milliseconds.
 */

#include "hal.h"
#include "rbpindefinition.h"
#include "rbgroundsensor.h"
#include "rbbluetooth.h"

#define BLKTHRESHOLD  600u           // Cap discharge time to consider seen black
#define ICUCLKFREQ    1000000u       // 1MHz ICU clock frequency

struct gndSensor gndSensor;

/**
 * Callback by ICU when input reach low logic level
 * Check if ground color is black by measuring discharge time
 * Signal semaphore if black seen
 */
static void _capDischarged_callback(ICUDriver *icup) {
  if (icuGetWidthX(icup) > BLKTHRESHOLD) {
    gndSensor.black = true;
  } else {
    gndSensor.black = false;
  }
  chSysLockFromISR();
  chBSemSignalI(&gndSensor.sem);
  chSysUnlockFromISR();
}


/**
 * ICU configurations
 */
static ICUConfig icucfgL = {
  .mode = ICU_INPUT_ACTIVE_HIGH,
  .frequency = ICUCLKFREQ,
  .width_cb = _capDischarged_callback,
  .channel = GNDSS_ICU_CHAN_L
};

/**
 * API - Start measuring time for reflectance sensor to discharge (non blocking)
 * Set pins as output high
 * Sleep briefly to let capacitor charge fully
 * Set pin as input, ICU should capture rising edge immediately and will callback on falling edge
 */
void rbGNDsensorStart(void) {
  palSetLineMode(GNDSS_PIN_L, PAL_MODE_OUTPUT_PUSHPULL);   //Set pins as output high (default output is high)
  chSysPolledDelayX(US2RTC(STM32_SYSCLK,20));              //Sleep briefly to let capacitor charge fully
  palSetLineMode(GNDSS_PIN_L, GNDSS_PALMODE_L);            //Set pin as input, ICU starts and will callback on falling edge
}

/**
 * API - Start ICU for timing measurements of reflectance sensor
 */
void rbGNDsensorInit(void) {
  chBSemObjectInit(&gndSensor.sem, true);
  icuStart(&GNDSS_ICU_DRV_L, &icucfgL);
  icuStartCapture(&GNDSS_ICU_DRV_L);
  icuEnableNotifications(&GNDSS_ICU_DRV_L);
}

