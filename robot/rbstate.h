/*
 * robot.h
 *  Created on: Mar 15, 2019
 *      Author: Phongbach
 */

/*
 * Define a type rbstate_t with various data and methods
 * Used for keeping track of robot state machine
 * No interaction with hardware
 */
#ifndef RBSTATE_H
#define RBSTATE_H

#include "ch.h"

/* Motor spin directions */
#define SPINFWD 0u
#define SPINREV 1u

/* Robot states of maneuver */
typedef enum {
    FWD = 0u,
    TURNRIGHT,
    TURNBACK,
    PAUSE,
    STOP
} rbState_name;

extern mutex_t rbStMutex;

/**
 * Various current statuses of robot
 */
typedef volatile struct {
  rbState_name curState;
  rbState_name prevState;
  float disRight;
  float disFront;
  float err;
  float Kp;
  float Ki;
  float Kd;
  float gain;
  float brake;
  float spdLm;
  float spdRm;
  unsigned int dirLm;
  unsigned int dirRm;
  unsigned int blackline;
} rbstate_t;

/**
 * All rbState members that need explicit initial value need to be done here.
 */
void rbSt_Init(rbstate_t *rbStatep);

/**
 * Update robot PID constants, received from commands
 * Output: false - invalid values; true - success
 */
bool rbSt_PIDtune(rbstate_t *rbStatep, float Kp, float Ki, float Kd);

/**
 * Update robot distance calculated from distance sensors value
 */
void rbSt_DisUpdate(rbstate_t *rbStatep, float disFront, float disRight);

/**
 * Use distance sensors values to update various parameters of robot state accordingly
 */
void rbSt_Update(rbstate_t *rbStatep);

/**
 * State after updated will be start
 */
void rbSt_Stop(rbstate_t *rbStatep);

/**
 * State after updated will be forward
 */
void rbSt_Start(rbstate_t *rbState);

/**
 * Check if robot is stopped
 */
bool rbSt_isStopped(rbstate_t *rbStatep);

#endif /* RBSTATE_H */
